#!/usr/bin/env bash
FILES=$(go list ./...)
exec golint -set_exit_status "$FILES"
